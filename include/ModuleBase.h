#ifndef MODULEBASE_H
#define MODULEBASE_H

#include <string>
#include "../interface/Versioning/include/Versioning.h"
#include <memory>
#include "../interface/SimpleJson/json.hpp"

#ifdef _WIN64
#  define EXT_C extern "C" __declspec( dllexport ) int __fastcall
#elif _WIN32
#  define EXT_C extern "C" __declspec( dllexport ) int
#else
#  define EXT_C extern "C" int
#endif

class ModuleBase {
public:
	ModuleBase(std::string const &name, bool const ownThread, uint8_t const priority, Version const version);
	ModuleBase(std::shared_ptr<json::JSON const> const config);
	virtual ~ModuleBase();
	ModuleBase();
	
	virtual bool Configure();
	virtual bool Init();
	virtual bool Start();
	virtual bool Stop();

	std::string const &Name();


protected:
	std::string _name;
	uint8_t _priority;
	Version _version;
	bool _ownThread;

	std::shared_ptr<json::JSON const> _config;
};

typedef ModuleBase * (*MOD_Factory)();
#endif // !MODULEBASE_H