#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include "../interface/SimpleJson/json.hpp"
#include "../include/ModuleLoader.h"

class ProcessManager {
public:
	ProcessManager::ProcessManager(std::string const & path);
	void StartSystem();
	void StopSystem();

	bool LoadModules();

	bool Boot();
	bool LoadConfig();

public:
	std::unique_ptr<ModuleLoader> _modLoader;
	std::string _configPath;
	std::shared_ptr<json::JSON> _modulesConfig;
	std::vector<std::shared_ptr<ModuleBase>> _modules;
};

#endif // !PROCESSMANAGER_H