#ifndef MODULELOADER_H
#define MODULELOADER_H

#include "../include/ModuleBase.h"
#include "../interface/SimpleJson/json.hpp"
#include <memory>
#include <vector>

#if defined(WIN32) || defined(MINGW32) || defined(MINGW64)
#include <Windows.h>

#define DLL_LOAD(dir)	LoadLibrary(dir.c_str())
#define DLL_ADDRESS		GetProcAddress
#define DLL_INSTANCE	HINSTANCE
#define DLL_FREE		FreeLibrary
#define DLL_ERROR		""

#else

#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define DLL_LOAD(dir)	dlopen(dir, RTLD_LAZY)
#define DLL_ADDRESS		dlsym
#define DLL_INSTANCE	void*
#define DLL_FREE		dlclose
#define DLL_ERROR		dlerror()

#endif



#ifdef _DEBUG || DEBUG
const std::string CONFIGSTR = "debug";
#else 
const std::string CONFIGSTR = "release";
#endif // _DEBUG || DEBUG


class ModuleLoader{
public:
	ModuleLoader::ModuleLoader();

	bool Load(std::shared_ptr<json::JSON const> const modulesCfg, std::vector<std::shared_ptr<ModuleBase>> &modules);
	bool Unload();

	bool ConfigureModules();
	bool InitModules();
	bool StartModules();
	bool StopModules();

private:
	std::string _moduleBaseDirectory;
	std::vector<std::shared_ptr<ModuleBase>> _modules;
};

#endif // !MODULELOADER_H

_declspec(dllexport) std::shared_ptr<ModuleBase> CreateModule();