#include "../include/ProcessManager.h"
#include "../lib/SysLog/include/Syslog.h" 
#include <memory>

#if defined(WIN32)|| defined(MINGW32) || defined(MINGW6)
#include <Windows.h>

#else
#include <csignal>
#include <unistd.h>
#include <stdlib.h>

#endif

ProcessManager::ProcessManager(std::string const& path) :
	_configPath(path),
	_modulesConfig(nullptr),
	_modLoader(std::make_unique<ModuleLoader>())
{}

void ProcessManager::StartSystem() {
	if (Boot())
		SysLog::Log("Boot successful", true);
	else {
		SysLog::Log("Failure booting");
		return;
	}

	if (LoadModules())
		SysLog::Log("Modules loading successful", true);
	else {
		SysLog::Log("Failure loading modules");
	}

}

void ProcessManager::StopSystem() {
	SysLog::Log("Stopping system");
	_modLoader->Unload();
}

bool ProcessManager::LoadModules() {

	SysLog::Log("Loading modules...");
	return _modLoader->Load(_modulesConfig, _modules);
}

bool ProcessManager::Boot() {

	SysLog::Log("System Booting...");

#if defined(WIN32)|| defined(MINGW32) || defined(MINGW6)

	SysLog::Log("Windows platform detected");

#else

	SysLog::Log("Linux platform detected");

#endif

	SysLog::Log("Loading Config...");

	bool successBoot = LoadConfig();

	if (!successBoot)
		return false;

	SysLog::Log("Config Loaded");

	return true;
}

bool ProcessManager::LoadConfig() {
	json::JSON configFile = json::JSON::LoadFromFile(_configPath);

	_modulesConfig = std::make_shared<json::JSON>(configFile["Modules"]);

	if (_modulesConfig->length() > 0)
		return true;

	return false;
}