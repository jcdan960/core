#include "../include/ModuleLoader.h"
#include "../lib/SysLog/include/Syslog.h"

/*
inspired from:
https://stackoverflow.com/questions/431533/c-dynamically-loading-classes-from-dlls
and
https://stackoverflow.com/questions/8696653/dynamically-load-a-function-from-a-dll
and
https://stackoverflow.com/questions/57805122/how-to-create-an-object-from-a-dll-file-in-c-at-runtime/57807076#57807076
*/

ModuleLoader::ModuleLoader() {
	
	_moduleBaseDirectory = std::string(BINARY_DIRECTORY) + "//" + std::string(CONFIGSTR) + "//";
	
}

bool ModuleLoader::Load(std::shared_ptr<json::JSON const> const modulesCfg, std::vector<std::shared_ptr<ModuleBase>> &modules) {

	for (size_t i = 0; i < modulesCfg->length();++i) {
		auto config = std::make_shared<json::JSON>(modulesCfg->at(i));

		std::string const modName = (*config)["name"].ToString();

		std::string const logStr = "Loading module " + modName;
		SysLog::Log(logStr);
		auto path = _moduleBaseDirectory + (*config)["path"].ToString();;

		DLL_INSTANCE moduleToLoad = DLL_LOAD(path);

		if (!moduleToLoad) {
			SysLog::Log("Failed to load module " + modName);
			continue;
		}

		auto mod = reinterpret_cast<MOD_Factory>(DLL_ADDRESS(moduleToLoad, modName.c_str()));;

		ModuleBase* modPtr = reinterpret_cast<ModuleBase*>(DLL_ADDRESS(moduleToLoad, modName.c_str()));

		//auto mod2 = std::make_shared<ModuleBase>(modPtr);


		if (!mod) {
			SysLog::Log("Could not locate symbols for module " + modName);
			continue;
		}


		//auto mod = CreateModule();

		//	std::shared_ptr<ModuleBase> mod2 = std::make_shared<ModuleBase>(test);

			//auto mod = (ModuleBase*)DLL_ADDRESS(moduleToLoad, modName.c_str());

		modules.emplace_back(std::make_shared<ModuleBase>(config));

		SysLog::Log("Module " + modName + " sucessfully loaded");
	}

	return true;
}

bool ModuleLoader::Unload() {
	StopModules();
	return true;
}

bool ModuleLoader::ConfigureModules() {
	for (auto &r : _modules) {
		SysLog::Log("Configuring module " + r->Name());
	}
	return true;
}

bool ModuleLoader::InitModules() {
	return true;
}

bool ModuleLoader::StartModules() {
	return true;
}

bool ModuleLoader::StopModules() {
	return true;
}

_declspec(dllexport) std::shared_ptr<ModuleBase> CreateModule() {
	return std::make_shared<ModuleBase>();
}

ModuleBase* CreateNewModule() {
	return new ModuleBase;
}

