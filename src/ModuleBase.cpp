#include "../include/ModuleBase.h"

ModuleBase::ModuleBase(std::string const& name, bool const ownThread, uint8_t const priority, Version const version) :
	_name(name), _ownThread(ownThread), _priority(priority), _version(version){

}

ModuleBase::~ModuleBase() {

}

ModuleBase::ModuleBase(std::shared_ptr<json::JSON const> const config) : _config(config){

	auto cfg = *config;

	_name = cfg["name"].ToString();

	int x2 = cfg["priority"].ToInt();
	float x3 = cfg["version"].ToFloat();

	auto x = cfg["ownThread"];
	_ownThread = cfg["ownThread"].ToBool();

}

ModuleBase::ModuleBase() : _name("unknown"), _ownThread(false), _priority(-1), _version(Version()) {

}

bool ModuleBase::Configure() {
	return true;
}

bool ModuleBase::Init() {
	return true;
}

bool ModuleBase::Start() {
	return true;
}

bool ModuleBase::Stop() {
	return true;
}

std::string const &ModuleBase::Name() {
	return _name;
}

